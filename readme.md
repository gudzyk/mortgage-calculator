# Mortgage calculator

Mortgage calculator is a web application which can help create bank list with the mortgage payment conditions and then calculate mortgage monthly payments according to the terms of the banks.

## Installation

Web application use localStorage of web browser to store data, so to install app user need download archive, unpack it to the web server folder and go to the link [http://127.0.0.1/calculator/index.html](http://127.0.0.1:5500/calculator/index.html) or user can go to the link [https://phenomenal-parfait-8fba0a.netlify.app/index.html](https://phenomenal-parfait-8fba0a.netlify.app/index.html)

## Usage

#### Bank management page

On this page user can see the list of the earlier created banks with their payment terms and go to the mortgage calculator page. 
User can also add new bank terms to the list with **Add** button, edit existing list with **Edit** button, remove active row in the list with **Delete** button, make bank list uneditable and save data to LocalStorage of web browser with **Save** button. 

#### Mortgage calculator page

On this page user can enter initial loan and down payment sums and choose bank from dropdown list (which includes banks from bank management page). After pressing **Calculate** button the checking of entered data and selected bank terms will be done, application will calculate and display monthly mortgage payment or will show error message.
User can also go to the bank management page.

## License

[MIT](https://choosealicense.com/licenses/mit/)