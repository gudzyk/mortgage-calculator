
let activeRowIndex;
selectedfunc();
loadfunc();

// Highlight active table row
function selectedfunc() {
    var table = document.getElementById('banktable');
    var cells = table.getElementsByTagName('td');

    for (var i = 5; i < cells.length; i++) {
        var cell = cells[i];
        // do something on onclick event for cell
        cell.onclick = function () {
            var rowId = this.parentNode.rowIndex;
            if (rowId > 0) {
                var rowsNotSelected = table.getElementsByTagName('tr');
                for (var row = 0; row < rowsNotSelected.length; row++) {
                    rowsNotSelected[row].style.backgroundColor = "";
                    rowsNotSelected[row].classList.remove('selected');
                }
                var rowSelected = table.getElementsByTagName('tr')[rowId];
                activeRowIndex = rowSelected.rowIndex;
                rowSelected.className += " selected";
            }                    
        }
    }
}

// Add new row to table
function addfunc() {
    var table = document.getElementById("banktable");
    var totalRowCount = table.rows.length;

    // insert row and cells after last row
    var row = table.insertRow(totalRowCount);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);

    // incert inputs in new cells
    var input1 = document.createElement("input");
    var input2 = document.createElement("input");
    var input3 = document.createElement("input");
    var input4 = document.createElement("input");
    var input5 = document.createElement("input");
    cell1.appendChild(input1);
    cell2.appendChild(input2);
    cell3.appendChild(input3);
    cell4.appendChild(input4);
    cell5.appendChild(input5);
    input1.type = "text";
    input2.type = "number";
    input3.type = "number";
    input4.type = "number";
    input5.type = "number";
    input2.className = "input-col2";
    input3.className = "input-col2";
    input4.className = "input-col2";
    input5.className = "input-col2";
    selectedfunc();
}
    
// Edit table data (make inputs editable)
function editfunc() {    
    var table = document.getElementById('banktable');
    var cells = table.getElementsByTagName('input');
    for (var i = 0; i < cells.length; i++) {
            cells[i].readOnly = false;
        }
    }

// Delete active table row
function delfunc() {
        var table = document.getElementById("banktable");
        if (activeRowIndex != undefined) table.deleteRow(activeRowIndex);
        activeRowIndex = undefined;
}
    
// Save table data (make inputs readonly, put data in Local storage)
function savefunc() {
    var table = document.getElementById('banktable');
    var cells = table.getElementsByTagName('input');
    cells = Array.from(cells);
    values = [];
    banks = [];
    for (var i=0; i<cells.length; i++) {
        //check empty cells in table
        if (cells[i].value == "") {
            alert("You have some empty cells");
            return;
        }
        // add inputs values in array
        values.push(cells[i].value);
        // add bank names in array 2
        if (i%5 == 0) banks.push(cells[i].value);
        cells[i].readOnly = true;
    }
    localStorage.clear();
    // put arrays in Local storage
    localStorage.setItem("values",JSON.stringify(values));
    localStorage.setItem("banks", JSON.stringify(banks));
}

// Build page, load data from Local storage
function loadfunc() {
    // get data from Local storage
    var stored = JSON.parse(localStorage.getItem("values"));
    var rowsCount = stored.length/5;
    var table = document.getElementById('banktable');
    // add table rows for data from Local storage
    for (var i=1; i<rowsCount; i++) {
        var row = table.insertRow(i);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var input1 = document.createElement("input");
        var input2 = document.createElement("input");
        var input3 = document.createElement("input");
        var input4 = document.createElement("input");
        var input5 = document.createElement("input");
        cell1.appendChild(input1);
        cell2.appendChild(input2);
        cell3.appendChild(input3);
        cell4.appendChild(input4);
        cell5.appendChild(input5);
        input1.type = "text";
        input2.type = "number";
        input3.type = "number";
        input4.type = "number";
        input5.type = "number";
        input2.className = "input-col2";
        input3.className = "input-col2";
        input4.className = "input-col2";
        input5.className = "input-col2";
        selectedfunc();
    }
    // put data in inputs in table
    var cells = table.getElementsByTagName('input');
    if (rowsCount > 0) {
        for (var i = 0; i < cells.length; i++) {
            cells[i].value = stored[i];
            cells[i].readOnly = true;
        }
    }
}

