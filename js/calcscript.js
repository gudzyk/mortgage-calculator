
banklistfunc();

// Get bank list from Local Storage
function banklistfunc() {
    var myParent = document.getElementById("bank-option");
    var array = JSON.parse(localStorage.getItem("banks"));
    // create and append select list
    var selectList = document.getElementById("banks");
    for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        selectList.appendChild(option);
    }
}

// Calculate monthly payment
function calcfunc() {
    // get data from Local storage and inputs in page
    var values = JSON.parse(localStorage.getItem("values"));
    var bank = document.getElementById("bank-list").value;
    var loanSum = document.getElementById("loan-sum").value;
    var downPayment = document.getElementById("down-payment").value;

    // get payment conditions of selected bank
    for (var i=0; i<values.length; i++) {
        if (values[i] == bank) {
            var rate = parseFloat(values[i+1]) / 100;
            var maxLoanSum = parseFloat(values[i+2]);
            var minDownPayment = parseFloat(values[i+3]);
            var term = parseFloat(values[i+4]) * 12;
        }
    }
    var resultMessage = "";
    // check empty fields on page
    if (loanSum.length == 0) { resultMessage = "You do not put initial loan sum"; }
    else {
        if (downPayment == 0) { resultMessage = "You do not put down payment sum"; }
        else {
            if (bank == "") { resultMessage = "You do not select bank"; }
            else {
                loanSum = parseFloat(loanSum);
                downPayment = parseFloat(downPayment);
                // compare loan and down payment sums
                if (loanSum <= downPayment) { resultMessage = "Your down payment sum must be smaller than initial loan sum"; } 
                else {
                    // compare initial loan and max loan sums
                    if (loanSum > maxLoanSum) {
                        resultMessage = "Your initial loan sum is bigger than maximum loan sum in selected bank ("+maxLoanSum+")";
                    }
                    else {
                        // compare initial down payment and min down payment sums
                        if (downPayment < minDownPayment * loanSum / 100) {
                            resultMessage = "Your down payment sum is smaller than minimum down payment sum in selected bank ("+minDownPayment+"%)";
                        }
                        else {
                            // calculate monthly payment
                            var result = parseFloat((loanSum - downPayment) * (rate / 12) * (1 + rate / 12) ** term / ((1 + rate / 12) ** term - 1)).toFixed(2);
                            resultMessage = "Monthly payment, USD: ";
                        }
                    }
                }
            }
        }
    }
    
    if (result > 0) {
        // display result if all checks were positive
        var myParent = document.getElementById("result-message");
        myParent.innerHTML = "";
        var resultText = document.createElement("span");
        resultText.id = "result-is";
        myParent.appendChild(resultText);
        resultText.textContent = resultMessage;
        var resultInput = document.createElement("input");
        resultInput.id = "result-input";
        myParent.appendChild(resultInput);
        resultInput.type = "number";
        resultInput.readOnly = true;
        resultInput.value = result;   
    }
    // display message about negative check
    else { document.getElementById("result-message").textContent = resultMessage; }    
}